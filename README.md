# README #

## What is this repository for? ###

* `service-a` is a simple micro-service that displays information about itself and pings another service(service-b).
* current version `0.0.1-SNAPSHOT`
* container name `cinchremit/service-a`
* stable tag `master`

## How do I get set up? ###

* Install java 11, maven, docker and kubernetes
* There are 2 profiles which can be run `local-dev` & `default`.
* `local-dev` should be used when running in container based enviroments(`docker` or `kubernetes`).
* `default` should be used when developing locally.
* * The interface dependency below is hosted on my private maven repository.
```xml
<dependency>
  <groupId>com.innovationlabs</groupId>
  <artifactId>interface</artifactId>
  <version>1.0-SNAPSHOT</version>
</dependency>
```
Included in this project is a template file maven settings with the needed entries. You will just need to update the {{MAVEN_REPO_PASSWORD}} fields with the proper credentials and then point
your maven settings to this file in order to run this project locally with maven. The credentials were sent in the submission email. You can request them again by email: mgbahacho@aol.com
If you only intend to run through containers, then this step isn't necessary.
* If you decide to use the [start script](start-dependencies.sh) or [manual build script](manual-build.sh), it will most likely fail because of 2 reasons
- you haven't pointed in the right maven file(see [The point above on maven](#how-do-i-get-set-up))
  To solve this, see the point above about maven interface dependency.
- you don't have access to the docker repository pointed in these files.
  To solve this, you need to run the following steps
1. update to app name to match a docker repository you have access to. Change `app_name="cinchremit/service-a"` in the [manual build script](manual-build.sh) to the right docker repository name.
```
app_name="<right repository name>/service-a"
```
2. Update the `pod.spec.containers` section of the [workload-template-file](service-a-workload-template.yaml) to point to the right image
```yaml
containers:
        - envFrom:
          - configMapRef:
              name: service-a-config
          image: <right repository>/service-a:{{BRANCH}}
          name: service-a
```
3. If the image is `private` in your repository, you should change the settings to `public`.
4. If you'd rather not do that, then you need to create a secret and update the `pod.spec.imagePullSecrets` section of [workload-template-file](service-a-workload-template.yaml) with a secret containing access to your image repository.
- create a secret called reg-cred
```
### update the values placeholders with the real values.
kubectl create secret docker-registry regcred --docker-server=<your-registry-server> --docker-username=<your-name> --docker-password=<your-pword> --docker-email=<your-email> -n innovationlabs
```
- update the workload file
```yaml
spec:
  imagePullSecrets:
  - name: docker-regcred
  containers:
#rest of file omitted for brevity...
```
5. All set, you should be able to run [start script](start-dependencies.sh) or [manual build script](manual-build.sh) without issues.

#### Dependencies
* The following services are needed when running in a container environment.
  * `Elastic search` --> `port 9200`
  * `Kibana` --> `port 5601`
  * `Logstash` --> `port 5000`
  * `Service B` --> `ports 8081, 9091`

## Running The Application
There are 3 simple ways to run all services.
1. Git clone this repo into the system/node you intend to run these files. You can apply the needed files. You can use the [start script](start-dependencies.sh) to deploy all files in the right order.
```
git clone https://cinch-remit@bitbucket.org/cinch-remit/k8s-manifests.git
cd k8s-manifests
sh start-dependencies.sh
```
2. If you'd rather not git clone the k8s repository, you could copy the contents of [deploy application script](https://bitbucket.org/cinch-remit/k8s-manifests/src/master/deploy-application-from-repo.sh) into a local file with `.sh` extension and run the script.(You should have kubernetes setup on your system).
```
cat <<EOF >localfile.sh
kubectl delete -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/dev-namespace.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/dev-namespace.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/elk/elastic-search-workload.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/elk/kibana-workload.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/elk/logstash-config.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/elk/logstash-workload.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/innovationlabs/service-a-workload.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/innovationlabs/service-b-workload.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/innovationlabs/gateway-workload.yaml
EOF 

sh localfile.sh
```
3. If you'd rather not create a file at all, you can just copy the text below directly into your terminal.
```
{
echo "delete previous namespace"
kubectl delete -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/dev-namespace.yaml

echo "creating namespace"
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/dev-namespace.yaml

echo "starting Elastic Logstash..."
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/elk/elastic-search-workload.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/elk/kibana-workload.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/elk/logstash-config.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/elk/logstash-workload.yaml

echo "starting innovationlabs services..."
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/innovationlabs/service-a-workload.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/innovationlabs/service-b-workload.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/innovationlabs/gateway-workload.yaml
}
```

### Running in local-dev profile
Use the [start dependencies script](start-dependencies.sh) to start all services for `local-dev` environment.
Use the [start dependencies script](start-dependencies.sh) to start all services for `local-dev` environment.

* The [start dependencies script](start-dependencies.sh) generates a local workload file for your current branch and restarts all deployments. This script should be run if it is desired to push changes to repository before restarting the application.
```
git clone https://cinch-remit@bitbucket.org/cinch-remit/service-a.git
cd service-a
sh start-dependencies.sh
```
* If the container with the current branch already exists, the running [build script](manual-build.sh) will package and push your changes to dockerhub. You can then restart the service-a application using `kubectl rollout restart deployment service-a -n innovationlabs`
```
sh manual-build.sh
kubectl rollout restart deployment service-a -n innovationlabs
```
* The container is stored with docker repository with the name `cinchremit/service-a`.
* This application runs on ports 8080(`http`) and 9090(`grpc`) but can be changed by adding the environment variable `server.port=<port number>` in the env section of the `service-a-workload.yaml`.
```yaml
env:
- name: server.port
  value: <http port number>
  
- name: grpc.server.port
  value: <grpc port number>
```
Update the container ports section of the `service-a-workload.yaml` file
```yaml
ports:
  - containerPort: <http port number>
    name: http
  - containerPort: <grpc port number>
    name: grpc
```
Update the ports section of the gateway-svc section as well in the `service-a-workload.yaml` file
```yaml
ports:
  - name: port-1
    port: <http port number>
    protocol: TCP
    targetPort: <http port number>

  - name: port-2
    port: <grpc port number>
    protocol: TCP
    targetPort: <grpc port number>
```

A much easier way to change the port without having to change the deployment file is to update the service-a-svc targetPort section to the desired ports
```yaml
ports:
  - name: port-1
    port: 8080
    protocol: TCP
    targetPort: <http port number>
    
  - name: port-2
    port: 9090
    protocol: TCP
    targetPort: <grpc port number>
```

### Running in default profile
You can run the application from your IDE or alternatively from the terminal using `mvn spring-boot:run`

### How to run tests
Use maven to run tests (`mvn test`)

## Endpoints.
### Grpc
The communication between the internal services and the gateway is done via `grpc`.
* app info: 
```
grpcurl --plaintext localhost:9090 com.innovationlabs.lib.AppInfoService.GetAppInfo
```
Remember to change localhost:port to the appropriate value for your deployment.
* health check: 
```
grpcurl --plaintext localhost:9090 grpc.health.v1.Health.Check
```
Remember to change localhost:port to the appropriate value for your deployment.

* internal communication: this command can be used to trigger an internal communication with service b: 
```
grpcurl --plaintext -d '{"from": "service-b", "message" : "hello"}' localhost:9090 com.innovationlabs.lib.InterServiceComm.Communicate
```
Remember to change localhost:port to the appropriate value for your deployment.

## Internal communication
This service has scheduled job which pings another service(`service b`). It is currently set to run every minute but can be configured to run differently.
By changing the cron value either in the application.yaml file while running in localdev, or injecting the values as env into the manifest files when running `local-dev`.
```yaml
interservice:
  ping:
    message: ping
    cron: 0 0/1 * 1/1 * ?
```
Internal communication can be monitored via logs in kibana and the output should look something like this

```json
{
  "_index": "logstash-2021.11.07-000001",
  "_type": "_doc",
  "_id": "sHQn-XwBxW2_JynzYWc1",
  "_version": 1,
  "_score": null,
  "_source": {
    "logger_name": "com.innovationlabs.servicea.service.sender.InterServiceSenderImpl",
    "message": "Got response {com.innovationlabs.lib.InterServiceReply.processed=true, com.innovationlabs.lib.InterServiceReply.reply=Received message ==    ping @ 2021-11-07T06:47:59.932084    == from service-a @ 2021-11-07T06:47:59.939212} from service-b",
    "spanId": "f1c2ff1d34c5ce14",
    "host": "10-1-5-162.service-a-svc.innovationlabs.svc.cluster.local",
    "port": 39230,
    "tags": [
      "innovationLabs"
    ],
    "@timestamp": "2021-11-07T06:47:59.943Z",
    "level": "INFO",
    "@version": "1",
    "traceId": "f1c2ff1d34c5ce14",
    "thread_name": "scheduling-1",
    "level_value": 20000,
    "application_name": "service-a"
  },
  "fields": {
    "@timestamp": [
      "2021-11-07T06:47:59.943Z"
    ]
  },
  "sort": [
    1636267679943
  ]
}
```
Or run `grpcurl --plaintext -d '{"from": "service-b", "message" : "hello"}' localhost:9090 com.innovationlabs.lib.InterServiceComm.Communicate` to trigger an internal communication.
## Logs
### local-dev
When running the container environment, logs can be monitored through the kibana interface which is found at path `http://localhost:5601/kibana`. Remember to change localhost:port to the appropriate value for your deployment.
By adding the `logstash-logback-encoder` in the [pom file](pom.xml), and configuring the [logback configuration](src/main/resources/logback.xml) to point to the logstash endpoint, we are able to send logs directly from our app to logstash.
```xml
<!--logstash-logback-encoder dependency-->
<dependency>
  <groupId>net.logstash.logback</groupId>
  <artifactId>logstash-logback-encoder</artifactId>
  <version>6.6</version>
</dependency>
```
```xml
<!-- logstash config part of logback.xml -->
<appender name="logstash" class="net.logstash.logback.appender.LogstashTcpSocketAppender">
  <destination>logstash_host:logstash_port</destination>
  <encoder class="net.logstash.logback.encoder.LogstashEncoder"/>
</appender>
```
This simplifies the process of sending logs through logstash to elastic-search.
### default
When running in default, logs can be seen through console.

## Improvements
Some ideas on how to improve this service.
### Configuration Server
Place configurations for the application in a central place which can be accessed by the application at startup.
The application does not need to be rebuilt for config changes. The application only needs to be restarted for changes to take effect.
### Security(Authentication & Authorization)
The application at this moment is open and anyone can access it. By leveraging spring security the application can be secured in a way that only authenticated users can access the application and authenticated users can only perform actions they are authorized to do.
### Tracing
With microservices, calls will be made across various services, finding a way to track these calls can be of value for monitoring all systems.
A great tool is [zipkin](https://zipkin.io).

### Who do I talk to? ###

* Repo owner or admin
  * Robinson Mgbah - mgbahacho@aol.com

