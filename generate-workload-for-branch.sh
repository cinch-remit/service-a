#!/bin/bash

# substitute the {{BRANCH}} template in the template file with the value of the branch passed in first argument.
template=`cat "service-a-workload-template.yaml" | sed "s/{{BRANCH}}/$1/g"`

# run a dry-run to generate the file.
echo "$template" | kubectl apply -f - --dry-run=client -o yaml > service-a-workload.yaml