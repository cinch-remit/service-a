#!/bin/bash

mvn clean package

branch_name="$(git symbolic-ref HEAD 2>/dev/null)" ||
branch_name="(unnamed branch)"     # detached HEAD

branch_name=${branch_name##refs/heads/}


jarfile="/target/service-a.jar"
app_name="cinchremit/service-a"

echo "building docker image: $app_name manually using jarfile: $jarfile && version: $branch_name"

docker image build --build-arg JAR_FILE=$jarfile -t $app_name:$branch_name .
docker image push $app_name:$branch_name