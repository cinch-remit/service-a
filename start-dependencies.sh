#!/bin/bash
echo "delete previous namespace"
kubectl delete -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/dev-namespace.yaml

branch_name="$(git symbolic-ref HEAD 2>/dev/null)" ||
branch_name="(unnamed branch)"     # detached HEAD

branch_name=${branch_name##refs/heads/}

echo "packaging gateway for current branch $branch_name"
sh manual-build.sh

echo "creating namespace"
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/dev-namespace.yaml

echo "starting Elastic Logstash..."
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/elk/elastic-search-workload.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/elk/kibana-workload.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/elk/logstash-config.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/elk/logstash-workload.yaml

echo "starting innovationlabs services..."

kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/innovationlabs/gateway-workload.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/innovationlabs/service-b-workload.yaml

echo "generating workload file for $branch_name branch"
sh generate-workload-for-branch.sh "$branch_name"

kubectl apply -f service-a-workload.yaml