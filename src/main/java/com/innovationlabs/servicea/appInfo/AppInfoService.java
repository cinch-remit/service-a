package com.innovationlabs.servicea.appInfo;

import com.innovationlabs.lib.AppInfoReply;
import com.innovationlabs.lib.AppInfoRequest;
import com.innovationlabs.lib.AppInfoServiceGrpc;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;
import org.springframework.beans.factory.annotation.Value;

/**
 * @author Robinson Mgbah
 * Date: 2021-11-05
 * Time: 21:35
 */
@GrpcService
public class AppInfoService extends AppInfoServiceGrpc.AppInfoServiceImplBase {
    @Value("${git.branch}")
    private String branch;

    @Value("${git.build.version}")
    private String buildVersion;

    @Value("${spring.application.name}")
    private String applicationName;

    @Value("${git.build.time}")
    private String lastBuildTime;

    @Value("${git.commit.time}")
    private String lastCommitTime;

    @Value("${git.commit.user.email}")
    private String email;

    @Value("${git.commit.user.name}")
    private String name;

    /**
     * Receives a request for app information and returns the appropriate app information.
     * @param request the request to be handled {@link AppInfoRequest}
     * @param responseObserver the response to be sent {@link StreamObserver<AppInfoReply>}
     */
    @Override
    public void getAppInfo(AppInfoRequest request, StreamObserver<AppInfoReply> responseObserver) {
        var reply = AppInfoReply.newBuilder()
                .setBranch(branch)
                .setBuildVersion(buildVersion)
                .setAppName(applicationName)
                .setCommitTime(lastCommitTime)
                .setName(name)
                .setEmail(email)
                .setLastBuildTime(lastBuildTime)
                .build();

        responseObserver.onNext(reply);
        responseObserver.onCompleted();
    }
}
