package com.innovationlabs.servicea.configuration.logging;

import net.devh.boot.grpc.server.interceptor.GrpcGlobalServerInterceptor;
import org.springframework.context.annotation.Configuration;

/**
 * @author Robinson Mgbah
 * Date: 2021-11-05
 * Time: 21:57
 */
@Configuration
public class GrpcLoggingConfiguration {

    @GrpcGlobalServerInterceptor
    GrpcLogInterceptor logInterceptor() {
        return new GrpcLogInterceptor();
    }
}
