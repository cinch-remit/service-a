package com.innovationlabs.servicea.job;

import com.innovationlabs.lib.InterServiceRequest;
import com.innovationlabs.servicea.service.sender.InterServiceSender;
import io.grpc.StatusRuntimeException;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * @author Robinson Mgbah
 * Date: 2021-11-06
 * Time: 01:30
 */
@Component
@Slf4j
public class InterServiceCommunicateJob {

    private final InterServiceSender interServiceSender;

    @Setter
    @Value("${spring.application.name}")
    private String from;

    @Setter
    @Value("${interservice.ping.message}")
    private String message;

    public InterServiceCommunicateJob(InterServiceSender interServiceSender) {
        this.interServiceSender = interServiceSender;
    }

    /**
     * Sends a message to service-b using grpc.
     */
    @Scheduled(cron = "${interservice.ping.cron}")
    public void buzzServiceB() {
        var pingRequest = InterServiceRequest.newBuilder()
                .setFrom(from)
                .setMessage(message + " @ " + LocalDateTime.now())
                .build();

        try {
            interServiceSender.send(pingRequest);
        }catch (StatusRuntimeException e) {
            log.error("Could not ping service", e);
        }catch (Exception e){
            log.error("Something went wrong...", e);
        }
    }
}
