package com.innovationlabs.servicea.service.sender;

import com.innovationlabs.lib.InterServiceCommGrpc;
import com.innovationlabs.lib.InterServiceReply;
import com.innovationlabs.lib.InterServiceRequest;
import lombok.extern.slf4j.Slf4j;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.springframework.stereotype.Service;

/**
 * @author Robinson Mgbah
 * Date: 2021-11-06
 * Time: 00:28
 */
@Service
@Slf4j
public class InterServiceSenderImpl implements InterServiceSender {

    @GrpcClient("service-b")
    private InterServiceCommGrpc.InterServiceCommBlockingStub interServiceCommBlockingStub;

    /**
     * Sends an interconnection request to service-b.
     * @param request The request to be sent to the server {@link InterServiceRequest}.
     * @return The response received from the server {@link InterServiceReply}
     */
    @Override
    public InterServiceReply send(InterServiceRequest request) {
        log.info("sending communication with message {} from {} to service-b", request.getMessage(), request.getFrom());
        var reply = interServiceCommBlockingStub.communicate(request);
        log.info("Got response {} from service-b", reply.getAllFields());

        return reply;
    }
}
