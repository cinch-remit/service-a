package com.innovationlabs.servicea.service.receiver;

import com.innovationlabs.lib.InterServiceCommGrpc;
import com.innovationlabs.lib.InterServiceReply;
import com.innovationlabs.lib.InterServiceRequest;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import net.devh.boot.grpc.server.service.GrpcService;

import java.time.LocalDateTime;

/**
 * @author Robinson Mgbah
 * Date: 2021-11-06
 * Time: 00:19
 */
@GrpcService
@Slf4j
public class InterServiceReceiver extends InterServiceCommGrpc.InterServiceCommImplBase {
    /**
     * Receives a request from a server and returns a reply acknowledging receipt.
     * @param request The request to be handled {@link InterServiceRequest}
     * @param responseObserver Stream observer which will house the response {@link StreamObserver<InterServiceReply>}.
     */
    @Override
    public void communicate(InterServiceRequest request, StreamObserver<InterServiceReply> responseObserver) {
        log.info("received communication {} from {}", request.getMessage(), request.getFrom());
        var reply = InterServiceReply.newBuilder()
                .setProcessed(true)
                .setReply("Received message ==    "  + request.getMessage() + "    == from " + request.getFrom() + " @ " + LocalDateTime.now())
                .build();

        responseObserver.onNext(reply);
        responseObserver.onCompleted();
    }
}
