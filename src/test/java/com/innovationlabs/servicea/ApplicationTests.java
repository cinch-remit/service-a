package com.innovationlabs.servicea;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(properties = {
		"grpc.server.inProcessName=test", // Enable inProcess server
		"grpc.server.port=-1", // Disable external server
		"grpc.client.inProcess.address=in-process:test" // Configure the client to connect to the inProcess server
})
class ApplicationTests {

	@Autowired
	ApplicationContext ctx;

	@Test
	void contextLoads() {
		assertNotNull(ctx);
	}

}
