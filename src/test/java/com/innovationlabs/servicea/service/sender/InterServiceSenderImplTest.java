package com.innovationlabs.servicea.service.sender;

import com.innovationlabs.lib.InterServiceCommGrpc;
import com.innovationlabs.lib.InterServiceReply;
import com.innovationlabs.lib.InterServiceRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * @author Robinson Mgbah
 * Date: 2021-11-07
 * Time: 14:13
 */
class InterServiceSenderImplTest {

    @Mock
    InterServiceCommGrpc.InterServiceCommBlockingStub interServiceCommBlockingStub;

    @InjectMocks
    InterServiceSenderImpl interServiceSender;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void send() {
        var from = "testing client";
        var message = "testing message";

        var request = InterServiceRequest.newBuilder()
                .setFrom(from)
                .setMessage(message)
                .build();

        when(interServiceCommBlockingStub.communicate(any(InterServiceRequest.class)))
                .thenReturn(InterServiceReply.newBuilder()
                        .setProcessed(true)
                        .setReply("Got your message")
                        .build());

        var reply = interServiceSender.send(request);
        assertNotNull(reply);

        verify(interServiceCommBlockingStub, times(1))
                .communicate(request);
    }
}