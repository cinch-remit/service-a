package com.innovationlabs.servicea.job;

import com.innovationlabs.lib.InterServiceRequest;
import com.innovationlabs.servicea.service.sender.InterServiceSender;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * @author Robinson Mgbah
 * Date: 2021-11-07
 * Time: 14:11
 */
class InterServiceCommunicateJobTest {

    @Mock
    InterServiceSender interServiceSender;

    @InjectMocks
    InterServiceCommunicateJob job;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        job.setFrom("from");
        job.setMessage("message");
    }

    @Test
    void buzzServiceA() {
        job.buzzServiceB();

        verify(interServiceSender, times(1))
                .send(any(InterServiceRequest.class));
    }
}